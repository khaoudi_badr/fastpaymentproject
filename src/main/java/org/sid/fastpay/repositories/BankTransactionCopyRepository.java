package org.sid.fastpay.repositories;

import org.sid.fastpay.entities.BankTransactionCopy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankTransactionCopyRepository extends JpaRepository<BankTransactionCopy, Long> {

}
