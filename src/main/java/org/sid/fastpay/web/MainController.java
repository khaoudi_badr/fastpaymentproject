package org.sid.fastpay.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.sid.fastpay.entities.BankTransaction;
import org.sid.fastpay.entities.BankTransactionCopy;
import org.sid.fastpay.entities.Role;
import org.sid.fastpay.entities.User;
import org.sid.fastpay.repositories.BankTransactionCopyRepository;
import org.sid.fastpay.repositories.BankTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

	@Autowired
	private BankTransactionCopyRepository bankTransactionRepository;
	
	@GetMapping("/login")
	public String login() {
		return("login");
	}
	
	@GetMapping("/")
	public String home(Model model,HttpServletRequest request) {
		
		List<BankTransactionCopy>bankTransactions= bankTransactionRepository.findAll();
		model.addAttribute("transactions",bankTransactions);
		return "index";
	}
}
