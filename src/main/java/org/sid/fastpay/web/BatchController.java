package org.sid.fastpay.web;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sid.fastpay.entities.BankTransaction;
import org.sid.fastpay.entities.BankTransactionCopy;
import org.sid.fastpay.repositories.BankTransactionCopyRepository;
import org.sid.fastpay.repositories.BankTransactionRepository;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class BatchController {
	
	@Autowired
	private JobLauncher jobLauncher;
	@Autowired
	private Job job;
	
	@Autowired
	private BankTransactionCopyRepository bankTransactionRepository;
	
	@GetMapping("/startJob")
	public String load() throws Exception {
		
		Map<String, JobParameter> params=new HashMap<>();
		params.put("time",new JobParameter(System.currentTimeMillis()));
		JobParameters jobParameters=new JobParameters(params);
		JobExecution jobExecution=jobLauncher.run(job, jobParameters);
		
		while(jobExecution.isRunning()) {
			System.out.println("....");
		}
		 System.out.println(jobExecution.getStatus());
		 
		 List<BankTransactionCopy> bankTransactions=bankTransactionRepository.findAll();
		 for(int i=0;i<bankTransactions.size();i++) {
			 bankTransactions.get(i).setAmount(bankTransactions.get(i).getAmount()*2);
		 }
		 bankTransactionRepository.saveAll(bankTransactions);
		 
		 return "redirect:/";
		
		
		
		
	}
	
	

}
