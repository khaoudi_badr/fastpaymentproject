package org.sid.fastpay.services;

import org.sid.fastpay.entities.User;
import org.sid.fastpay.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {
	
	User save(UserRegistrationDto registrationDto);
	
	

}
