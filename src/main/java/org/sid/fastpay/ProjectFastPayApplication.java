package org.sid.fastpay;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.sid.fastpay.entities.BankTransaction;
import org.sid.fastpay.entities.BankTransactionCopy;
import org.sid.fastpay.entities.Role;
import org.sid.fastpay.entities.User;
import org.sid.fastpay.repositories.BankTransactionCopyRepository;
import org.sid.fastpay.repositories.BankTransactionRepository;
import org.sid.fastpay.repositories.UserRepository;
import org.sid.fastpay.services.UserServiceImpl;
import org.sid.fastpay.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
public class ProjectFastPayApplication implements CommandLineRunner {

	
	@Autowired
	private BankTransactionRepository bankTransactionRepository;
	
	@Autowired
	private BankTransactionCopyRepository bankTransactionCopyRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(ProjectFastPayApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		List<BankTransaction> bankTransactions=bankTransactionRepository.findAll();
		for(int i=0;i<bankTransactions.size();i++) {
			BankTransactionCopy bank=new BankTransactionCopy(bankTransactions.get(i).getAccountID()+1, bankTransactions.get(i).getTransactionDate(), bankTransactions.get(i).getStrTransactionDate(), bankTransactions.get(i).getTransactionType(),bankTransactions.get(i).getAmount() );
			bankTransactionCopyRepository.save(bank);
		}
		
		
		
	}

}
